package com.zhou.jvm.loading.testfour;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-22:52
 */
public class ClassLoadingProcedure {
    public static void main(String[] args) {
        System.out.println(T.count);
    }
}
class  T{
    public static int count = 2; // 0
    public static T t = new T(); // null

    private T(){
        count++;
    }
}