package com.zhou.jvm.loading.testfour;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-22:58
 */
public class ClassLoadingStatic {
    public static void main(String[] args) {
        System.out.println(Test.a);
    }
}
class Test{
    public Test test = new Test();
    public static int a = 0;

    static{
        a = 5;
    }
    private Test(){
        a++;
    }
}