package com.zhou.jvm.loading.testtwo;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-9:05
 */
public class ParentAndChild {
    public static void main(String[] args) {
        ParentAndChild p = new ParentAndChild();
        // class com.zhou.jvm.testtwo.ParentAndChild
        System.out.println(p.getClass());
        // sun.misc.Launcher$AppClassLoader@18b4aac2
        System.out.println(p.getClass().getClassLoader());
        // class sun.misc.Launcher$AppClassLoader
        System.out.println(p.getClass().getClassLoader().getClass());
        // sun.misc.Launcher$ExtClassLoader@1b6d3586
        System.out.println(p.getClass().getClassLoader().getParent());
        // class sun.misc.Launcher$ExtClassLoader
        System.out.println(p.getClass().getClassLoader().getParent().getClass());
        // 抛空指针异常
//        System.out.println(p.getClass().getClassLoader().getParent().getParent().getClass());

        // 这里已经到null
        /*
          系统类加载器的父加载器是标准扩展类加载器，
          但是我们试图获取标准扩展类加载器的父类加载器时却得到了null。
          事实上，由于启动类加载器无法被Java程序直接引用，
          因此JVM默认直接使用 null 代表启动类加载器。*/
        System.out.println(ClassLoader.getSystemClassLoader());
        System.out.println(p.getClass().getClassLoader().getParent().getClass().getClassLoader());
        // Exception in thread "main" java.lang.NullPointerException
//        System.out.println(p.getClass().getClassLoader().getParent().getClass().getClassLoader().getParent());
    }
}
