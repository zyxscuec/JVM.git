package com.zhou.jvm.loading.testthree;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-17:23
 */
public class LazyLoading {
    public static void main(String[] args) throws ClassNotFoundException {
//        P p;
//        X x = new X();
//        System.out.println(P.i);
//        System.out.println(P.j);
        Class.forName("com.zhou.jvm.loading.testthree.LazyLoading$P");
    }

    public static class P {
        final static int i = 8;
        static int j = 9;
        static {
            System.out.println("P");
        }
    }

    public static class X extends P {
        static {
            System.out.println("X");
        }
    }
}
