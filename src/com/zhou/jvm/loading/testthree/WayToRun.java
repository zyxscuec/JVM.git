package com.zhou.jvm.loading.testthree;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-21:27
 */
public class WayToRun {
    public static void main(String[] args) {
        for (int i = 0; i < 10_0000L; i++) {
            m();
        }

        long start =  System.currentTimeMillis();

        for (int i = 0; i < 10_0000L; i++) {
            m();
        }

        long end = System.currentTimeMillis();
        System.out.println("Time :" + (end - start));
    }

    public static void m(){
        for (int i = 0; i < 10_0000L; i++) {
            long j = i % 3;
        }
    }
}
