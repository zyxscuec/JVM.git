package com.zhou.jvm.loading.testthree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-21:32
 * 破坏双亲委派机制
 */
public class ClassLoadOverride {

    private static class MyLoader extends ClassLoader {
        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {

//            File f = new File("C:/work/ijprojects/JVM/out/production/JVM/" + name.replace(".", "/").concat(".class"));
            String filePath = "D:\\WorkSpace\\IdeaWorkSpace\\JVM-zhou-master\\out\\production\\JVM-zhou-master\\";
            File f = new File(filePath + name.replace(".", "/").concat(".class"));
            if(!f.exists()) {
                return super.loadClass(name);
            }


            try {

                InputStream is = new FileInputStream(f);

                byte[] b = new byte[is.available()];
                is.read(b);
                return defineClass(name, b, 0, b.length);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return super.loadClass(name);
        }
    }

    public static void main(String[] args) throws Exception {
        MyLoader m = new MyLoader();
        Class clazz = m.loadClass("com.zhou.jvm.Hello");

        m = new MyLoader();
        Class clazzNew = m.loadClass("com.zhou.jvm.Hello");

        System.out.println(clazz == clazzNew);
    }
}
