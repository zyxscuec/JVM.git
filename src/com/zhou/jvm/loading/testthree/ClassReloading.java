package com.zhou.jvm.loading.testthree;

import com.zhou.jvm.loading.testtwo.OverrideClassLoad;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/27-21:38
 * 双亲委派机制：无论new 多少个classload，只要name相同，生成的hashcode就会是一样的
 */
public class ClassReloading {
    public static void main(String[] args) throws ClassNotFoundException {
        OverrideClassLoad classLoad = new OverrideClassLoad();
        Class<?> loadClass = classLoad.loadClass("com.zhou.jvm.Hello");

        classLoad = null;
        System.out.println(loadClass.hashCode());

        OverrideClassLoad classLoad2 = new OverrideClassLoad();
        Class<?> loadClass1 = classLoad2.loadClass("com.zhou.jvm.Hello");
        System.out.println(loadClass1.hashCode());
        System.out.println(loadClass == loadClass1);
    }
}
