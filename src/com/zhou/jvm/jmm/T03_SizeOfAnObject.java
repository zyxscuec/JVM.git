package com.zhou.jvm.jmm;

import com.zhou.jvm.objectsize.ObjectSizeAgent;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/5-18:07
 */
public class T03_SizeOfAnObject {
    public static void main(String[] args) {
        System.out.println(ObjectSizeAgent.sizeOf(new Object()));
        System.out.println(ObjectSizeAgent.sizeOf(new int[] {}));
        // 注释中写的 8 + 4 + 4 + 4 + 1 + 1 + 4 + 1 = 31 还要加上对象对齐1 所以输出32
        System.out.println(ObjectSizeAgent.sizeOf(new P()));
    }

    //一个Object占多少个字节
    // -XX:+UseCompressedClassPointers -XX:+UseCompressedOops
    // Oops = ordinary object pointers
    private static class P {
        //8 _markword
        //4 _class pointer
        int id;         //4
        // 引用类型压缩为 4 byte
        String name;    //4
        int age;        //4

        byte b1;        //1
        byte b2;        //1

        Object o;       //4
        byte b3;        //1

    }
}
