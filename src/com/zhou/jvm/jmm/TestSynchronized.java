package com.zhou.jvm.jmm;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/5-17:12
 */
public class TestSynchronized {
    synchronized void m(){

    }
    void n(){
        synchronized (this){}{

        }
    }


    public static void main(String[] args) {

    }
}
