package com.zhou.jvm.runtimedataAreainstructionset;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/6-10:54
 */
public class T03_InvokeSpecial {

    public static void main(String[] args) {
        new T03_InvokeSpecial().n();
    }

    public final void n(){}

//    public void m(){}

}
