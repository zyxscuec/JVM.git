package com.zhou.jvm.runtimedataAreainstructionset;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/5-21:31
 */
public class TPlusPlusOther {
    public static void main(String[] args) {
        int i = 8;
        i = i++;
        int c = i++;
        System.out.println(c);
    }
}
