package com.zhou.jvm.runtimedataAreainstructionset;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/6-10:53
 */
public class T02_InvokeVirtual {

    public static void main(String[] args) {
        new T02_InvokeVirtual().m();
    }

    public void m(){}

}
