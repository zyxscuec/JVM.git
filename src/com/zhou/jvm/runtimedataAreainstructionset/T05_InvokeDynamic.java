package com.zhou.jvm.runtimedataAreainstructionset;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/6-10:45
 */
public class T05_InvokeDynamic {

    public static void main(String[] args) {
        I i1 = C::n;
        I i2 = C::n;
        I i3 = C::n;
        I i4 = ()->{
            C.n();
        };
        System.out.println(i1.getClass());
        System.out.println(i2.getClass());
        System.out.println(i3.getClass());
    }

    @FunctionalInterface
    public static interface I{
        void m();
    }

    public static class C{
        static void n(){
            System.out.println(" Hello ");
        }
    }


}
