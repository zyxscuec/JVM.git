package com.zhou.jvm.gc;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/6-21:37
 * //-XX:-DoEscapeAnalysis -XX:-EliminateAllocations -XX:-UseTLAB -Xlog:c5_gc*
 * // 逃逸分析 标量替换 线程专有对象分配
 */
public class TestTLAB {

    class User{

        int  id;
        String name;

        public User(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    void alloc(int i){
        new User(i,"name: " + i);
    }

    public static void main(String[] args) {
        TestTLAB t = new TestTLAB();
        long start = System.currentTimeMillis();
        for (int i = 0;i < 1000_0000L;i++){
            t.alloc(i);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

}
